'use strict';
const model = require('./model');

// factory for creating route handlers
function getHandler(getter) {
    return (request, reply) => {
        getter(request.params.hazardous)
        .then(data => {
            reply(data);
        })
        .catch(err => {
            console.error(err);
        });
    };
}

module.exports.register = function (server, options, next) {

    // hello world
    server.route({
        method: 'GET',
        path: '/',
        handler: (request, reply) => {
            reply({ hello: 'world!' });
        }
    });

    // gets all the hazardous NEOs from the DB
    server.route({
        method: 'GET',
        path: '/neo/hazardous',
        handler: getHandler(model.getHazardous)
    });

    // gets the fastest NEO from the DB
    // if hazardous query string is passed - gets the fastest
    // hazardous NEO
    server.route({
        method: 'GET',
        path: '/neo/fastest',
        handler: getHandler(model.getFastest)
    });

    // gets the year that had the most NEOs in it
    // if hazardous query string is passed - gets the year
    // where most hazardous NEOs were
    server.route({
        method: 'GET',
        path: '/neo/best-year',
        handler: getHandler(model.getBestYear)
    });

    // gets the month that had the most NEOs in it
    // if hazardous query string is passed - gets the month
    // where most hazardous NEOs were
    server.route({
        method: 'GET',
        path: '/neo/best-month',
        handler: getHandler(model.getBestMonth)
    });

    server.route({
        method: 'PUT',
        path: '/neo/add/{date?}',
        handler: getHandler(model.getBestMonth)
    });

    next();
};

module.exports.register.attributes = {
    name: 'api',
    version: '1.0.0'
};
