'use strict';

const moment = require('moment');
const db = require('./dbAccess');

let collection = db.then(_db => {
    collection = _db.collection('documents');
});


function getAggregationPipeline(isHazardous, groupByExpression) {
    const aggPipeline = [];
        if (isHazardous) {
            aggPipeline.push({ $match: { isHazardous: true }});
        }
        aggPipeline.push({
            $group: {
                _id: groupByExpression,
                count: { $sum: 1 }
            }
        });
        return collection.aggregate(aggPipeline).sort({ count: -1 }).toArray();
}

module.exports = {
    // returns all NEOs from the DB which are hazerdous
    getHazardous() {
        return collection.find({}).filter({ isHazardous: true }).toArray();
    },
    // returns the fastest NEO
    getFastest(isHazardous) {
        let cursor = collection.find();
        if (isHazardous) {
            cursor = cursor.filter({ isHazardous: true });
        }
        return cursor.sort({speed: -1}).limit(1).toArray();
    },
    getBestYear(isHazardous) {
        return getAggregationPipeline(isHazardous, { $year: "$date" })
        .then(data => {
            if (data.length === 0) { return data; }
            return {
                bestYear: data[0]._id,
                totalNeos: data[0].count
            };
        });
    },
    getBestMonth(isHazardous) {
        return getAggregationPipeline(isHazardous, { $month: "$date" })
        // format the data for easier read
        .then(data => {
            if (data.length === 0) { return data; }
            return {
                bestMonth: moment(data[0]._id, 'MM').format('MMMM'),
                totalNeos: data[0].count
            };
        });
    }
};
