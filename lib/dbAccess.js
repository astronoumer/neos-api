'use strict';

const config = require('config');
const MongoClient = require('mongodb').MongoClient;

// initialize connection to DB
let db = MongoClient.connect(config.get('db.host')) // eslint-disable-line prefer-const
    .then(_db => {
        console.log('connected');
        return _db;
    })
    .catch(err => {
        console.error(err);
        console.error('failed initializing db');
    });

module.exports = db;
