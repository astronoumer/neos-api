'use strict';
// const dbAccess = require('./lib/dbAccess');
const Hapi = require('hapi');

const server = new Hapi.Server();
server.connection({ port: 3000, host: 'localhost' });

server.register(require('./lib/api'), (err) => {
    if (err) {
        console.error('Failed to load plugin:', err);
    }
});


server.start((err) => {

    if (err) {
        throw err;
    }
    console.log(`Server running at: ${server.info.uri}`);
});
