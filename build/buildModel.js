'use strict';

const moment = require('moment');
const config = require('config');
const argv = require('minimist')(process.argv.slice(2)); //eslint-disable-line no-magic-numbers

const rp = require('request-promise');

require('../lib/dbAccess')
.then(db => {
    buildModel(db);
});

// the number of days we're building the dataset from. Default
// is taken from conifg but can be overriden by command line param
const days = argv.days || config.get('neoApi.numberOfDays');


// gets an index (X) and a date, and returns a moment
// X days back from that date

function buildModel(db) {
    getNEOs(days)
    .then(parseNEOs)
    .then(data => {
        return db.collection('documents').insertMany(data);
    })
    .then(res => {
        console.log(`Inserted ${res.ops.length} documents into the collection`);
    })
    .catch(err => {
        console.error(err);
        console.error('failed building model');
    });
}

function getXDaysAgo(date, numberOfDays) {
    return moment(date).subtract(numberOfDays, 'days');
}

// fetch data for the last X days
// recursive because the nasa api only allows 6 at a time
function getNEOs(_days, data, date) {
    const maxDays = config.get('neoApi.maxDaysAllowed');
    data = data || [];
    date = date || moment();
    const options = {
        uri: config.get('neoApi.feedURI'),
        qs: {
            api_key: config.get('neoApi.key')
        },
        json: true // Automatically parses the JSON string in the response
    };
    if (_days > maxDays) {
        options.qs.start_date = getXDaysAgo(date, 0).format('YYYY-MM-DD');
        options.qs.end_date = getXDaysAgo(date || moment(), maxDays).format('YYYY-MM-DD');
        console.log(options);
        return rp(options).then(_data => {
            if (_data && _data.near_earth_objects) {
                data.push(_data.near_earth_objects);
            }
            return getNEOs(_days - maxDays - 1, data, getXDaysAgo(date || moment(), maxDays + 1));
        });
    } else {
        options.qs.start_date = getXDaysAgo(date, 0).format('YYYY-MM-DD');
        options.qs.end_date = getXDaysAgo(date || moment(), _days).format('YYYY-MM-DD');
        return rp(options).then(_data => {
            if (_data && _data.near_earth_objects) {
                data.push(_data.near_earth_objects);
            }
            return data;
        });
    }
}

// gets information regarding the last 3 days
// and converts it to objects achording to the schema we want
function parseNEOs(data) {
    const res = [];
    for (const i in data) {
        const chunk = data[i];
        for (const date of Object.keys(chunk)) {
            const dateArr = chunk[date];
            // and pluck the relevant attributes
            if (dateArr) {
                dateArr.forEach(({
                    neo_reference_id: reference,
                    close_approach_data: cad,
                    name,
                    is_potentially_hazardous_asteroid: isHazardous
                }) => {
                    res.push({
                        date: cad.length > 0 ? new Date(cad[0].close_approach_date) : undefined,
                        reference,
                        name,
                        speed: cad.length > 0 && cad[0].relative_velocity ?
                            cad[0].relative_velocity.kilometers_per_hour :
                            undefined,
                        isHazardous
                    });
                });
            }
        }
    }
    console.log(res);
    return res;
}
