# NEOs Code challange

This repository was built as closely as possible to the requirements placed [here](https://github.com/mcmakler/basic-backend-interview-test/tree/2.1.2_nasa). I will here place a few assumptions I made will building performing this challange.

#### Setup: ####
1. The assumption is that when coming to work with the repo, you already have a mongodb instance running on the machine.
2. In order to have some data in the DB I wrote down a script called `buildModel.js` (under `build` folder). when run as-is, it fetches data for the last 3 days and puts it in the DB.
3. In order to create a larger model, I generalized the script so that you can pass to it a "days" param and by that increasing the data taken from nasa. So by running `node build/buildModel.js --days 1000`, you will get a dataset for the NEOs found in the last 1000 days.

#### Implementation comments and assumptions: ####
1. best-month and best-year are naive implementations. meaning I go through the entire dataset and count how many NEOs were found in year X or in month Y. Obviously statistically this is insufficiant as it assumes that the dataset is made up of complete years. If it isn't (for example if it has the entirety of 2015 and just one month of 2016) this calculation will be incorrect.
2. The server is build on top of Hapi, a node server framework that was design to put more focus on configuation and less on code. It is opinionated but robust and I like working with it for that reason. For this project specifically I might as well have used express but I'm more familiar with Hapi so I chose that.
3. Data validity / integrity - On a large project, when I define a model, I would also define a Schema (usually json schema) and then perform a validation before inserting to the DB. Here due to time limitations and some questions regarding the implementation (what is the expected result if the validation fails for a single NEO? the rest should be inserted?) I didn't get to it. Also, the insertion is very naive. It just performs an insert. It doesn't check first if there's already a NEO with the same reference. The assumption (for the purpose of this excercise), is that building the DB, and accessing it happens on different times in the application lifetime (an unrealistice assumption for any real life app).
