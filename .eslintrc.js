module.exports = {
    plugins: ['node'],
    extends: ["walmart/configurations/es6-node"],
    rules: {
        quotes: 0,
        'no-console': 0,
        'func-style': ['error', 'declaration'],
        'no-use-before-define': ['error', {
            'functions': false
        }],
        "no-invalid-this": 0,
        "node/exports-style": [ 'error', 'module.exports' ],
        // Enforce dash-cased filenames
        "filenames/match-regex": 0,
        // Match the file name against the default exported value in the module
        "filenames/match-exported": 0,
        // Don't allow index.js files
        "filenames/no-index": 0
    }

};